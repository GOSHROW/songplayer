package com.SongPlayer.config;

import com.SongPlayer.commands.*;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;
import com.SongPlayer.services.LoadSongData;

public class ApplicationConfig {

    private final String songPoolCSVPath = "./src/main/java/com/SongPlayer/repositories/data/songs.csv";
    private final LoadSongData loadSongData = new LoadSongData();    
    private final PoolRepository pool = loadSongData.CreatePool(songPoolCSVPath);
    private final UserRepository user = new UserRepository();

    private final ICommand createPlaylistCommand = new CreatePlaylistCommand();
    private final ICommand deletePlaylistCommand = new DeletePlaylistCommand();
    private final ICommand historyCommand = new HistoryCommand();
    private final ICommand listSongsCommand = new ListSongsCommand();
    private final ICommand modPlaylistCommand = new ModPlaylistCommand();
    private final ICommand mostPlayed10Command = new MostPlayed10Command();
    private final ICommand playPlaylistCommand = new PlayPlaylistCommand();
    private final ICommand playSongCommand = new PlaySongCommand();

    private final CommandInvoker commandInvoker = new CommandInvoker(pool, user);

    public CommandInvoker getCommandInvoker() {
        commandInvoker.register("CREATE-PLAYLIST", createPlaylistCommand);
        commandInvoker.register("DELETE-PLAYLIST", deletePlaylistCommand);
        commandInvoker.register("HISTORY", historyCommand);
        commandInvoker.register("LIST-SONG", listSongsCommand);
        commandInvoker.register("MODIFY-PLAYLIST", modPlaylistCommand);
        commandInvoker.register("MOST-PLAYED", mostPlayed10Command);
        commandInvoker.register("PLAY-PLAYLIST", playPlaylistCommand);
        commandInvoker.register("PLAY-SONG", playSongCommand);
        return commandInvoker;
    }

}