package com.SongPlayer.repositories;

import com.SongPlayer.entities.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class PoolRepository {
    private final HashMap<String, Genre> genreMap;
    private final HashMap<String, Artist> artistMap;
    private final HashMap<String, Album> albumMap;
    private final HashMap<Integer, Song> songMap;

    public PoolRepository() {
        genreMap = new HashMap<>();
        artistMap = new HashMap<>();
        albumMap = new HashMap<>();
        songMap = new HashMap<>();
    }

    /**
     * @return the albumMap
     */
    public HashMap<String, Album> getAlbumMap() {
        return albumMap;
    }
    /**
     * @return the artistMap
     */
    public HashMap<String, Artist> getArtistMap() {
        return artistMap;
    }
    /**
     * @return the genreMap
     */
    public HashMap<String, Genre> getGenreMap() {
        return genreMap;
    }
    /**
     * @return the songMap
     */
    public HashMap<Integer, Song> getSongMap() {
        return songMap;
    }

    public void addNewSong(Integer id, String songName, String genre, String album, String owner, ArrayList<String> artists) {
        addSongOb(id, songName, genre, album, owner, artists);
        addGenre(genre, id);
        addAlbum(album, id);
        if (!artists.contains(owner)) {
            addArtist(owner, id);
        }
        for (String artist: artists) {
            addArtist(artist, id);
        }
    }
    
    private void addSongOb(Integer id, String songName, String genre, String album, String owner, ArrayList<String>artists) {
        songMap.put(id, new Song(id, songName, genre, album, owner, artists));
    }

    private void addGenre(String genre, Integer songID) {
        if (genreMap.containsKey(genre)) {
            Genre genreOb = genreMap.get(genre);
            genreOb.addSong(songID);
        } else {
            ArrayList<Integer> songList = new ArrayList<>(Arrays.asList(songID));
            Genre genreOb = new Genre(genre, songList);
            genreMap.put(genre, genreOb);
        }
    }
    
    private void addAlbum(String album, Integer songID) {
        if (albumMap.containsKey(album)) {
            Album albumOb = albumMap.get(album);
            albumOb.addSong(songID);
        } else {
            ArrayList<Integer> songList = new ArrayList<>(Arrays.asList(songID));
            Album albumOb = new Album(album, songList);
            albumMap.put(album, albumOb);
        }
    }

    private void addArtist(String artist, Integer songID) {
        if (artistMap.containsKey(artist)) {
            Artist artistOb = artistMap.get(artist);
            artistOb.addSong(songID);
        } else {
            ArrayList<Integer> songList = new ArrayList<>(Arrays.asList(songID));
            Artist artistOb = new Artist(artist, songList);
            artistMap.put(artist, artistOb);
        }
    }

    private ArrayList<Song> getSongsList(ArrayList<Integer> songIDs) {
        ArrayList<Song> songList = new ArrayList<>();
        for (Integer songID : songIDs) {
            songList.add(songMap.get(songID));
        }
        return songList;
    }

    private void printSongsList(ArrayList<Integer> songIDs) {
        for (Integer songID : songIDs) {
            songMap.get(songID).prettyOut();
        }
    }

    public ArrayList<Song> getByGenre(String genre) {
        ArrayList<Integer> songIDs = new ArrayList<>();
        if (genreMap.containsKey(genre)) {
            songIDs = genreMap.get(genre).getSongList();
        }
        return getSongsList(songIDs);
    }
    public void printByGenre(String genre) {
        ArrayList<Integer> songIDs = new ArrayList<>();
        if (genreMap.containsKey(genre)) {
            songIDs = genreMap.get(genre).getSongList();
        }
        printSongsList(songIDs);
    }

    public ArrayList<Song> getByAlbum(String album) {
        ArrayList<Integer> songIDs = new ArrayList<>();
        if (albumMap.containsKey(album)) {
            songIDs = albumMap.get(album).getSongList();
        }
        return getSongsList(songIDs);
    }
    public void printByAlbum(String album) {
        ArrayList<Integer> songIDs = new ArrayList<>();
        if (albumMap.containsKey(album)) {
            songIDs = albumMap.get(album).getSongList();
        }
        printSongsList(songIDs);
    }

    public ArrayList<Song> getByArtist(String artist) {
        ArrayList<Integer> songIDs = new ArrayList<>();
        if (artistMap.containsKey(artist)) {
            songIDs = artistMap.get(artist).getSongList();
        }
        return getSongsList(songIDs);
    }
    public void printByArtist(String artist) {
        ArrayList<Integer> songIDs = new ArrayList<>();
        if (artistMap.containsKey(artist)) {
            songIDs = artistMap.get(artist).getSongList();
        }
        printSongsList(songIDs);
    }

    @Override
    public String toString() {
        return "Pool{" + 
            "songMap: " + new LinkedHashMap<>(songMap) + ", " +
            "genreMap: " + new LinkedHashMap<>(genreMap) + ", " +
            "albumMap: " + new LinkedHashMap<>(albumMap) + ", " +
            "artistMap: " + new LinkedHashMap<>(artistMap) + ", " + 
            "}";
    }

}