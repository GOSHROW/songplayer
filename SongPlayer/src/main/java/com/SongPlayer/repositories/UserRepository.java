package com.SongPlayer.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.SongPlayer.entities.*;
import com.SongPlayer.exceptions.PlaylistNotFound;
import com.SongPlayer.util.FrequencyMap;
import com.SongPlayer.util.LRUCache;

public class UserRepository {

    private HashMap<Integer, Playlist> playlistMap;
    private Integer currentPlaylist;
    private LRUCache history;
    private Integer newPlaylistID;
    private FrequencyMap frequencyMap;

    public UserRepository() {
        playlistMap = new HashMap<>();
        currentPlaylist = -1;
        history = new LRUCache(1000);
        newPlaylistID = 0;
        frequencyMap = new FrequencyMap();
    }

    public Integer createPlaylist(String name, ArrayList<Integer> songs) {
        playlistMap.put(newPlaylistID, new Playlist());
        playlistMap.get(newPlaylistID).create(newPlaylistID, name, songs);
        // switchPlaylist(newPlaylistID);
        newPlaylistID += 1;
        return newPlaylistID - 1;
    }

    public boolean deletePlaylist(Integer playlistID) {
        if (playlistMap.containsKey(playlistID)) {
            playlistMap.remove(playlistID);
            if (playlistID == currentPlaylist) {
                currentPlaylist = -1;
            }
            return true;
        }
        return false;
    }

    /**
     * @return the currentPlaylist
     */
    public Integer getCurrentPlaylist() {
        return currentPlaylist;
    }

    public HashMap<Integer, Playlist> getPlaylistMap() {
        return playlistMap;
    }
    public void switchPlaylist(Integer newPlaylistSelection) throws PlaylistNotFound {
        if (playlistMap.containsKey(newPlaylistSelection)) {
            currentPlaylist = newPlaylistSelection;
            playlistMap.get(currentPlaylist).playFirst();
        } else {
            throw new PlaylistNotFound();
        }
    }

    public void playedASong(Integer songID) {
        history.refer(songID);
        frequencyMap.addEntry(songID);
    }

    public ArrayList<Integer> getHistory() {
        ArrayList<Integer> retHistory = history.display();
        Collections.reverse(retHistory);
        return retHistory;
    }

    public List<List<Integer>> get10MostPlayed() {
        return frequencyMap.getNMaxFreqs(10);
    }
}