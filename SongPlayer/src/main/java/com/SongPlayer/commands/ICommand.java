package com.SongPlayer.commands;

import java.util.List;

import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

public interface ICommand {
    void execute(List<String> tokens, PoolRepository pool, UserRepository user) throws Exception;
}