package com.SongPlayer.commands;

import java.util.ArrayList;
import java.util.List;

import com.SongPlayer.services.CreatePlaylist;
import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

public class CreatePlaylistCommand implements ICommand {
    private CreatePlaylist createPlaylist;

    public CreatePlaylistCommand() {
        createPlaylist = new CreatePlaylist();
    }

    public void execute(List<String> tokens, PoolRepository pool, UserRepository user) throws SongNotInPool,
            ExpectedMoreArgs {
        
        if(tokens.size() < 2) {
            throw new ExpectedMoreArgs();
        }

        ArrayList<Integer> songIDs = new ArrayList<>();
        for(int i = 2; i < tokens.size(); ++i) {
            songIDs.add(Integer.parseInt(tokens.get(i)));
        }
        createPlaylist.createNewPlaylist(pool, user, tokens.get(1), songIDs);
    }
}