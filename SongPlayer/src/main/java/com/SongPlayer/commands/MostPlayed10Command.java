package com.SongPlayer.commands;

import java.util.List;

import com.SongPlayer.services.MostPlayed10;
import com.SongPlayer.repositories.*;


public class MostPlayed10Command implements ICommand{
    private MostPlayed10 mostPlayed10;
    public MostPlayed10Command() {
        mostPlayed10 = new MostPlayed10();
    }

    public void execute(List<String> tokens, PoolRepository pool, UserRepository user) {
        mostPlayed10.print10MostPlayed(pool, user);
    }
}