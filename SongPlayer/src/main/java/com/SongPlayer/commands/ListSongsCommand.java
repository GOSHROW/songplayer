package com.SongPlayer.commands;

import java.util.List;

import com.SongPlayer.services.*;
import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

public class ListSongsCommand implements ICommand {
    private SongsByAlbum songsByAlbum;
    private SongsByArtist songsByArtist;
    private SongsByGenre songsByGenre;

    public ListSongsCommand() {
        songsByAlbum = new SongsByAlbum();
        songsByArtist = new SongsByArtist();
        songsByGenre = new SongsByGenre();
    }

    public void execute(List<String> tokens, PoolRepository pool, UserRepository user) throws ExpectedMoreArgs,
            NoSuchCommandException {
        
        if(tokens.size() < 3) {
            throw new ExpectedMoreArgs();
        }

        if (tokens.get(1).equals("GENRE")) {
            songsByGenre.printSongsByGenre(pool, tokens.get(2));
        } else if (tokens.get(1).equals("ALBUM")) {
            songsByAlbum.printSongsByAlbum(pool, tokens.get(2));
        } else if (tokens.get(1).equals("ARTIST")) {
            songsByArtist.printSongsByArtist(pool, tokens.get(2));
        } else {
            throw new NoSuchCommandException();
        }

    }
}