package com.SongPlayer.commands;

import java.util.ArrayList;
import java.util.List;

import com.SongPlayer.services.ModPlaylist;
import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

public class ModPlaylistCommand implements ICommand {
    private ModPlaylist modPlaylist;

    public ModPlaylistCommand() {
        modPlaylist = new ModPlaylist();
    }

    public void execute(List<String> tokens, PoolRepository pool, UserRepository user) throws ExpectedMoreArgs,
            NoSuchCommandException, NumberFormatException, SongNotInPool, PlaylistNotFound {
        
        if(tokens.size() < 3) {
            throw new ExpectedMoreArgs();
        }

        ArrayList<Integer> songIDs = new ArrayList<>();
        for(int i = 3; i < tokens.size(); ++i) {
            songIDs.add(Integer.parseInt(tokens.get(i)));
        }

        if(tokens.get(1).equals("ADD-SONG")) {
            modPlaylist.addToPlaylist(pool, user, Integer.parseInt(tokens.get(2)), songIDs);
        } else if(tokens.get(1).equals("DELETE-SONG")) {
            modPlaylist.delFromPlaylist(pool, user, Integer.parseInt(tokens.get(2)), songIDs);
        } else {
            throw new NoSuchCommandException();
        }
    }
}