package com.SongPlayer.commands;

import java.util.List;

import com.SongPlayer.services.*;
import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

public class PlaySongCommand implements ICommand {
    private PlaySong playSong;
    private PlayPrev playPrev;
    private PlayNext playNext;

    public PlaySongCommand() {
        playSong = new PlaySong();
        playPrev = new PlayPrev();
        playNext = new PlayNext();
    }

    private static boolean isNumeric(String str) { 
        try {  
          Integer.parseInt(str);  
          return true;
        } catch(NumberFormatException e){  
          return false;  
        }  
    }

    public void execute(List<String> tokens, PoolRepository pool, UserRepository user) throws ExpectedMoreArgs,
            NoSongsInPlaylist, NoActivePlaylist, NoSuchCommandException, NumberFormatException, SongNotInPlaylist {
        
        if(tokens.size() < 2) {
            throw new ExpectedMoreArgs();
        }

        if (tokens.get(1).equals("NEXT")) {
            playNext.playNextSong(pool, user);
        } else if (tokens.get(1).equals("BACK")) {
            playPrev.playPrevSong(pool, user);
        } else if (isNumeric(tokens.get(1))) {
            playSong.playPlaylistSong(pool, user, Integer.parseInt(tokens.get(1)));
        } else {
            throw new NoSuchCommandException();
        }

    }
}