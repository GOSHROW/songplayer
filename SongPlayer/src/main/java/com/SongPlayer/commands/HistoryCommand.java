package com.SongPlayer.commands;

import java.util.List;

import com.SongPlayer.services.History;
import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

public class HistoryCommand implements ICommand {
    private History history;

    public HistoryCommand() {
        history = new History();
    }

    public void execute(List<String> tokens, PoolRepository pool, UserRepository user) throws ExpectedMoreArgs {
        
        if(tokens.size() < 2) {
            throw new ExpectedMoreArgs();
        }
        history.printHistory(pool, user, Integer.parseInt(tokens.get(1)));
    }
}