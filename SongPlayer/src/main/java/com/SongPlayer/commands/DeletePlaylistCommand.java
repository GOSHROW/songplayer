package com.SongPlayer.commands;

import java.util.List;

import com.SongPlayer.services.DeletePlaylist;
import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

public class DeletePlaylistCommand implements ICommand {
    private DeletePlaylist deletePlaylist;

    public DeletePlaylistCommand() {
        deletePlaylist = new DeletePlaylist();
    }

    public void execute(List<String> tokens, PoolRepository pool, UserRepository user) throws SongNotInPool,
            ExpectedMoreArgs, NumberFormatException, PlaylistNotFound {
        
        if(tokens.size() < 2) {
            throw new ExpectedMoreArgs();
        }

        deletePlaylist.DeleteAPlaylist(user, Integer.parseInt(tokens.get(1)));
    }
}