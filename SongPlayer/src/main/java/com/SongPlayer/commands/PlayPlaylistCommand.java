package com.SongPlayer.commands;

import java.util.List;

import com.SongPlayer.services.PlayPlaylist;
import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

public class PlayPlaylistCommand implements ICommand {
    private PlayPlaylist playPlaylist;

    public PlayPlaylistCommand() {
        playPlaylist = new PlayPlaylist();
    }

    public void execute(List<String> tokens, PoolRepository pool, UserRepository user) throws ExpectedMoreArgs,
            NumberFormatException, PlaylistNotFound, NoSongsInPlaylist {
        
        if(tokens.size() < 2) {
            throw new ExpectedMoreArgs();
        }

        playPlaylist.playAPlaylist(pool, user, Integer.parseInt(tokens.get(1)));
    }
}