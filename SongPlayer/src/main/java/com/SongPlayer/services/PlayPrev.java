package com.SongPlayer.services;

import com.SongPlayer.repositories.*;
import com.SongPlayer.entities.*;
import com.SongPlayer.exceptions.*;

public class PlayPrev {
    
    public void playPrevSong(PoolRepository pool, UserRepository user) throws NoSongsInPlaylist, NoActivePlaylist {
        if (user.getCurrentPlaylist() == -1) {
            throw new NoActivePlaylist();
        }
        Playlist currentPlaylist = user.getPlaylistMap().get(user.getCurrentPlaylist());
        if ( currentPlaylist.getSongs().size() == 0) {
            throw new NoSongsInPlaylist();
        }
        currentPlaylist.prevSong();
        user.playedASong(currentPlaylist.getCurrentSong());
        pool.getSongMap().get(currentPlaylist.getCurrentSong()).prettyOutPartial();
    }
}