package com.SongPlayer.services;

import com.SongPlayer.repositories.*;

import java.util.HashMap;
import java.util.List;

import com.SongPlayer.entities.*;

public class MostPlayed10 {

    public void print10MostPlayed(PoolRepository pool, UserRepository user) {
        List<List<Integer>> frequenciesOfMostPlayed = user.get10MostPlayed();
        HashMap<Integer, Song> songMap = pool.getSongMap();
        System.out.println("Songs   No of Plays");
        for (List<Integer> songIDFrequency: frequenciesOfMostPlayed) {
            System.out.println(songMap.get(songIDFrequency.get(0)).getSongName() + "  " + songIDFrequency.get(1));
        }
    }
}