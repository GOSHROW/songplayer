package com.SongPlayer.services;


import com.SongPlayer.repositories.*;
import com.SongPlayer.exceptions.*;

public class DeletePlaylist {

    public void DeleteAPlaylist(UserRepository user, Integer playlistID) throws PlaylistNotFound {
        if (user.deletePlaylist(playlistID)) {
            System.out.println("Delete Successful");
        } else {
            throw new PlaylistNotFound();
        }
    }
}