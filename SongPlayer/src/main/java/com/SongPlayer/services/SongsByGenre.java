package com.SongPlayer.services;

import com.SongPlayer.repositories.*;

public class SongsByGenre {
    public void printSongsByGenre(PoolRepository pool, String genre) {
        pool.printByGenre(genre);
    }    
}