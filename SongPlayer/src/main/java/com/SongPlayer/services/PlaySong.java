package com.SongPlayer.services;

import com.SongPlayer.repositories.*;
import com.SongPlayer.entities.*;
import com.SongPlayer.exceptions.*;

public class PlaySong {
    
    public void playPlaylistSong(PoolRepository pool, UserRepository user, Integer songID) throws SongNotInPlaylist, NoActivePlaylist {
        if (user.getCurrentPlaylist() == -1) {
            throw new NoActivePlaylist();
        }
        Playlist currentPlaylist = user.getPlaylistMap().get(user.getCurrentPlaylist());
        if ( !currentPlaylist.getSongs().contains(songID)) {
            throw new SongNotInPlaylist();
        }
        currentPlaylist.changeCurrent(songID);
        user.playedASong(currentPlaylist.getCurrentSong());
        pool.getSongMap().get(currentPlaylist.getCurrentSong()).prettyOutPartial();
    }
}