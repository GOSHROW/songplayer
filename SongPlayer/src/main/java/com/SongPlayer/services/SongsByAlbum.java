package com.SongPlayer.services;

import com.SongPlayer.repositories.*;

public class SongsByAlbum {
    public void printSongsByAlbum(PoolRepository pool, String album) {
        pool.printByAlbum(album);
    }    
}