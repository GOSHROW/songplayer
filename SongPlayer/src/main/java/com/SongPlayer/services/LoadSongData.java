package com.SongPlayer.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;

import com.SongPlayer.repositories.*;

public class LoadSongData {
    public PoolRepository CreatePool(String pathCSVStr) {
        try {
            PoolRepository retPool = new PoolRepository();
            BufferedReader br = new BufferedReader(new FileReader(pathCSVStr)); 
            String line; 
            String delim = ",";
            while ((line = br.readLine()) != null) {
                String[] attributes = line.split(delim);
                retPool.addNewSong(Integer.parseInt(attributes[0]), attributes[1], attributes[2], attributes[3], attributes[4], new ArrayList<>(Arrays.asList(attributes[5].split("#"))));
            }
            br.close();
            return retPool;
        } catch(Exception e) {
            e.printStackTrace();
            return new PoolRepository();
        }
    }
}