package com.SongPlayer.services;

import java.util.ArrayList;

import com.SongPlayer.repositories.*;
import com.SongPlayer.exceptions.*;
import com.SongPlayer.entities.*;

public class PlayPlaylist {
    public void playAPlaylist(PoolRepository pool, UserRepository user, Integer playlistID) throws PlaylistNotFound,
            NoSongsInPlaylist {
        user.switchPlaylist(playlistID);
        ArrayList<Integer> songsInSelectedPlaylist = user.getPlaylistMap().get(playlistID).getSongs();
        if (songsInSelectedPlaylist.size() == 0) {
            throw new NoSongsInPlaylist();
        }
        user.playedASong(songsInSelectedPlaylist.get(0));
        Song firstSong = pool.getSongMap().get(songsInSelectedPlaylist.get(0));
        firstSong.prettyOutPartial();
    }
}