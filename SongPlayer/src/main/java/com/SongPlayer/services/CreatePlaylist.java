package com.SongPlayer.services;

import java.util.ArrayList;

import com.SongPlayer.repositories.*;
import com.SongPlayer.exceptions.*;

public class CreatePlaylist {
    private PoolRepository pool;

    public CreatePlaylist() {
        pool = new PoolRepository();
    }

    public void createNewPlaylist(PoolRepository pool, UserRepository user, String name, ArrayList<Integer> songIDs) throws SongNotInPool{
        this.pool = pool;
        for(Integer songID : songIDs) {
            if (checkSongID(songID)) {
                continue;
            } else {
                throw new SongNotInPool();
            }
        }
        displayNewPlaylist(user.createPlaylist(name, songIDs));      
    }

    private void displayNewPlaylist(Integer playlistID) {
        System.out.println("Playlist ID - " + playlistID);
    }

    private boolean checkSongID(Integer songID) {
        return pool.getSongMap().containsKey(songID);
    }
}