package com.SongPlayer.services;

import java.util.ArrayList;
import java.util.HashMap;

import com.SongPlayer.repositories.*;
import com.SongPlayer.entities.*;

public class History {

    public void printHistory(PoolRepository pool, UserRepository user, Integer n) {
        ArrayList<Integer> historySongIDs = user.getHistory();
        HashMap<Integer, Song> songMap = pool.getSongMap();
        for (Integer songID: historySongIDs) {
            songMap.get(songID).prettyOut();
        }
    }
    
}