package com.SongPlayer.services;

import com.SongPlayer.repositories.*;

public class SongsByArtist {
    public void printSongsByArtist(PoolRepository pool, String artist) {
        pool.printByArtist(artist);
    }    
}