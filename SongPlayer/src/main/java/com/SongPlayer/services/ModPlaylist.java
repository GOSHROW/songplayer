package com.SongPlayer.services;

import java.util.ArrayList;
import java.util.HashMap;

import com.SongPlayer.entities.*;
import com.SongPlayer.repositories.*;
import com.SongPlayer.exceptions.*;

public class ModPlaylist {
    private PoolRepository pool;

    public ModPlaylist() {
        pool = new PoolRepository();
    }

    public void addToPlaylist(PoolRepository pool, UserRepository user, Integer playlistID, ArrayList<Integer> songIDs) throws SongNotInPool, PlaylistNotFound{
        this.pool = pool;
        for(Integer songID : songIDs) {
            if (checkSongID(songID)) {
                continue;
            } else {
                throw new SongNotInPool();
            }
        }
        HashMap<Integer, Playlist> playlistMap =  user.getPlaylistMap();
        if (playlistMap.containsKey(playlistID)) {
            Playlist playlist = playlistMap.get(playlistID);
            for (Integer songID: songIDs) {
                playlist.add(songID);
            }
            displayPlaylistChanges(playlist);
        } else {
            throw new PlaylistNotFound();
        }
    }


    public void delFromPlaylist(PoolRepository pool, UserRepository user, Integer playlistID, ArrayList<Integer> songIDs) throws SongNotInPool, PlaylistNotFound{
        this.pool = pool;
        for(Integer songID : songIDs) {
            if (checkSongID(songID)) {
                continue;
            } else {
                throw new SongNotInPool();
            }
        }
        HashMap<Integer, Playlist> playlistMap =  user.getPlaylistMap();
        if (playlistMap.containsKey(playlistID)) {
            Playlist playlist = playlistMap.get(playlistID);
            for (Integer songID: songIDs) {
                playlist.remove(songID);
            }
            displayPlaylistChanges(playlist);
        } else {
            throw new PlaylistNotFound();
        }
    }

    private void displayPlaylistChanges(Playlist playlist) {
        System.out.println("Playlist ID - " + playlist.getId());
        System.out.println("Playlist Name - " + playlist.getName());
        System.out.println("Song IDs - " + playlist.getSongs());
    }

    private boolean checkSongID(Integer songID) {
        return pool.getSongMap().containsKey(songID);
    }
}