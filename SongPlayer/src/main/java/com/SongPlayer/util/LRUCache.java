package com.SongPlayer.util;

import java.util.*;

public class LRUCache {
 
    Set<Integer> cache;
    int capacity;
 
    public LRUCache(int capacity)
    {
        this.cache = new LinkedHashSet<Integer>(capacity);
        this.capacity = capacity;
    }

    public boolean get(int key)
    {
        if (!cache.contains(key))
            return false;
        cache.remove(key);
        cache.add(key);
        return true;
    }
 
    public void refer(int key)
    {       
        if (get(key) == false)
           put(key);
    }
 
    public ArrayList<Integer> display()
    {
        ArrayList<Integer> list = new ArrayList<>(cache);
        return list;
    }
     
    public void put(int key)
    {
         
      if (cache.size() == capacity) {
            int firstKey = cache.iterator().next();
            cache.remove(firstKey);
        }
 
        cache.add(key);
    }
}