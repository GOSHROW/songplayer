package com.SongPlayer.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FrequencyMap {
    private Integer totalEntries;
    private HashMap<Integer, Integer> freqMap;

    public FrequencyMap() {
        freqMap = new HashMap<>();
        totalEntries = 0;
    }

    public void addEntry(Integer key) {
        if(!freqMap.containsKey(key)) {
            freqMap.put(key, 1);
        } else {
            freqMap.replace(key, freqMap.get(key) + 1);
        }
        totalEntries += 1;
    }

    public List<List<Integer>>  getNMaxFreqs(Integer n) {
        List<List<Integer>> freqArray = new ArrayList<List<Integer>>();
        for (Integer i = 0; i <= freqMap.size(); i++)
            freqArray.add(new ArrayList<Integer>());

        for (Map.Entry<Integer, Integer> entry : freqMap.entrySet()) {
            freqArray.get(entry.getValue()).add(entry.getKey());
        }

        List<List<Integer>> retFreqList = new ArrayList<List<Integer>>(); 
        for (Integer freq = freqMap.size(); freq > 0; --freq) {
            for (Integer keyEntries: freqArray.get(freq)) {
                retFreqList.add(new ArrayList<>(Arrays.asList(keyEntries, freq)));
                if (--n <= 0) {
                    break;
                }
            }
            if (n <= 0) {
                break;
            }
        }

        return retFreqList;
    }
}