package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Objects;

public class Artist {
    protected final String artistName;
    protected final ArrayList<Integer> songList;
    
    public Artist(String artistName) {
        this.artistName = artistName;
        this.songList = new ArrayList<>();
    }

    public Artist(String artistName, ArrayList<Integer> songList) {
        this.artistName = artistName;
        this.songList = songList;
    }

    protected String getArtistName() {
        return this.artistName;
    }

    public ArrayList<Integer> getSongList() {
        return this.songList;
    }

    public void addSong(Integer songID) {
        this.songList.add(songID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Artist)) return false;
        Artist artist = (Artist) o;
        return getArtistName().equals(artist.getArtistName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getArtistName());
    }

    @Override
    public String toString() {
        return "Artist{" +
            "artistName='" + artistName + '\'' +
            "songList='" + songList + '\'' +
            '}';
    }

}