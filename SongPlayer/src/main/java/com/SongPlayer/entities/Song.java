package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Objects;

public class Song {
    private final Integer id;
    private final String songName;
    private final String genre;
    private final String album;
    private final String owner;
    private final ArrayList<String> artists;

    public Song(Integer id, String songName, String genre, String album, String owner, ArrayList<String> artists) {
        this.id = id;
        this.songName = songName;
        this.genre = genre;
        this.album = album;
        this.owner = owner;
        this.artists = artists;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the songName
     */
    public String getSongName() {
        return songName;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @return the album
     */
    public String getAlbum() {
        return album;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @return the artists
     */
    public ArrayList<String> getArtists() {
        return artists;
    }
    
    /**
     * @return nothing; write well-defined output to screen for Pool Queries
     */
    public void prettyOut() {
        System.out.println(" Song ID \t- " + getId());
        System.out.println(" Song Name \t- " + getSongName());
        System.out.println(" Genre \t- " + getGenre());
        System.out.println(" Album \t- " + getAlbum());
        System.out.println(" Album Artist \t- " + getOwner());
        System.out.println(" Artists \t- " + String.join(",", getArtists()));
    }
    
    /**
     * @return nothing; write well-defined output to screen for Pool Queries
     */
    public void prettyOutPartial() {
        System.out.println(" Song ID \t- " + getId());
        System.out.println(" Song Name \t- " + getSongName());
        System.out.println(" Genre \t- " + getGenre());
        System.out.println(" Album \t- " + getAlbum());
        System.out.println(" Artists \t- " + String.join(",", getArtists()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Song)) return false;
        Song song = (Song) o;
        return getId().equals(song.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Song{" +
            "ID='" + id + '\'' +
            "songName='" + songName + '\'' +
            "genre='" + genre + '\'' +
            "album='" + album + '\'' +
            "owner='" + owner + '\'' +
            "artist='" + artists + '\'' +
            '}';
    }

}