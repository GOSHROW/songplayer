package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Objects;

public class Album {
    private final String albumName;
    private final ArrayList<Integer> songList;
    
    public Album(String albumName) {
        this.albumName = albumName;
        this.songList = new ArrayList<>();
    }

    public Album(String albumName, ArrayList<Integer> songList) {
        this.albumName = albumName;
        this.songList = songList;
    }

    private String getAlbumName() {
        return this.albumName;
    }

    public ArrayList<Integer> getSongList() {
        return this.songList;
    }

    public void addSong(Integer songID) {
        this.songList.add(songID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Album)) return false;
        Album album = (Album) o;
        return getAlbumName().equals(album.getAlbumName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAlbumName());
    }

    @Override
    public String toString() {
        return "Album{" +
            "albumName='" + albumName + '\'' +
            "songList='" + songList + '\'' +
            '}';
    }

}