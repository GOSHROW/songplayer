package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Objects;

public class Genre {
    private final String genreName;
    private final ArrayList<Integer> songList;
    
    public Genre(String genreName) {
        this.genreName = genreName;
        this.songList = new ArrayList<>();
    }

    public Genre(String genreName, ArrayList<Integer> songList) {
        this.genreName = genreName;
        this.songList = songList;
    }

    private String getGenreName() {
        return this.genreName;
    }

    public ArrayList<Integer> getSongList() {
        return this.songList;
    }

    public void addSong(Integer songID) {
        this.songList.add(songID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Genre)) return false;
        Genre genre = (Genre) o;
        return getGenreName().equals(genre.getGenreName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGenreName());
    }

    @Override
    public String toString() {
        return "Genre{" +
            "genreName='" + genreName + '\'' +
            "songList='" + songList + '\'' +
            '}';
    }

}