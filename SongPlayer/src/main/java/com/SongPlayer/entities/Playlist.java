package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Objects;

public class Playlist {
    Integer id;
    String name;
    Integer currentSong;
    ArrayList<Integer> songs;

    public Playlist() {
        id = -1;
        name = "";
        currentSong = -1;
        songs = new ArrayList<Integer>();
    }

    // Assuming ArratList is populated by atleast one entry.
    public Integer create(Integer id, String name, ArrayList<Integer> songs) {
        this.id = id;
        this.name = name;
        for (Integer song : songs) {
            this.songs.add(song);
        }
        return id;
    }

    public void playFirst() {
        currentSong = this.songs.get(0);
    }

    public ArrayList<Integer> add(Integer song) {
        if (!songs.contains(song)) 
            songs.add(song);
        return new ArrayList<>(songs);
    }
    public ArrayList<Integer> remove(Integer song) {
        if (songs.contains(song))
            songs.remove(song);
        if (song == currentSong)
            nextSong();
        return new ArrayList<>(songs);
    }

    public Integer changeCurrent(Integer song) {
        if (songs.contains(song))
            currentSong = song;
        return currentSong;
    }

    public Integer nextSong() {
        currentSong = this.songs.get((this.songs.indexOf(currentSong) + 1) % this.songs.size()); 
        return currentSong;
    }

    public Integer prevSong() {
        currentSong = this.songs.get((this.songs.indexOf(currentSong) - 1 + this.songs.size()) % this.songs.size());
        return currentSong;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the currentSong
     */
    public Integer getCurrentSong() {
        return currentSong;
    }

    /**
     * @return the songs
     */
    public ArrayList<Integer> getSongs() {
        return songs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Playlist)) return false;
        Playlist playlist = (Playlist) o;
        return getId().equals(playlist.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Playlist{" +
            "is='" + getId() + '\'' +
            "cuurentSong='" + getCurrentSong() + '\'' +
            '}';
    }
}