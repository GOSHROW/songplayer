package com.SongPlayer.exceptions;

public class SongNotInPool extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    
    @Override
    public String toString() {
        return "Some Requested Songs Not Available. Please try again.";
    }
}