package com.SongPlayer.exceptions;

public class NoSongsInPlaylist extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    
    @Override
    public String toString() {
        return "Playlist is empty";
    }
}