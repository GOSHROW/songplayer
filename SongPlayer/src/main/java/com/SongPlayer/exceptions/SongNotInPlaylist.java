package com.SongPlayer.exceptions;

public class SongNotInPlaylist extends Exception{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    
    @Override
    public String toString() {
        return "Song Not Found in the current active playlist.";
    }
}