package com.SongPlayer.exceptions;

public class PlaylistNotFound extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    
    @Override
    public String toString() {
        return "Playlist Not Found";
    }
}