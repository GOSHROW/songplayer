package com.SongPlayer.exceptions;

public class ExpectedMoreArgs extends Exception{
        /**
     *
     */
    private static final long serialVersionUID = 1L;

    
    @Override
    public String toString() {
        return "Expected More Args";
    }
}