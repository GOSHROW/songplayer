package com.SongPlayer.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.SongPlayer.commands.CommandInvoker;

@DisplayName("Integration Test")
public class IntegrationTest {

    @Test
    @DisplayName("check Integration Across Valid Commands")
    public void check_Integration_Across_Valid_Commands() throws Exception {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        CommandInvoker commandInvoker = applicationConfig.getCommandInvoker();
        ArrayList<String> commands = new ArrayList<>(); 
        commands.add("CREATE-PLAYLIST YOLO 1 3");
        commands.add("CREATE-PLAYLIST YOLD 1 3");
        commands.add("DELETE-PLAYLIST 1");
        commands.add("CREATE-PLAYLIST YOLDO 15 12");
        commands.add("MODIFY-PLAYLIST ADD-SONG 2 14 16");
        commands.add("MODIFY-PLAYLIST ADD-SONG 0 1");
        commands.add("PLAY-PLAYLIST 2");
        commands.add("PLAY-SONG 16");
        commands.add("PLAY-SONG NEXT");
        commands.add("PLAY-SONG NEXT");
        commands.add("PLAY-SONG NEXT");
        commands.add("PLAY-SONG BACK");
        commands.add("LIST-SONG GENRE Pop");
        commands.add("LIST-SONG ARTIST Queen");
        commands.add("LIST-SONG ALBUM Nevermind");
        commands.add("HISTORY 3");
        commands.add("MOST-PLAYED");

        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        
        for (String command: commands) {
            List<String> tokens = new ArrayList<String>(Arrays.asList(command.split(" ")));
            String commandName = tokens.get(0);
            System.out.println("\n" + command);
            commandInvoker.executeCommand(commandName, tokens);
        }
        
        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println("\nCREATE-PLAYLIST YOLO 1 3\nPlaylist ID - 0\n\nCREATE-PLAYLIST YOLD 1 3\nPlaylist ID - 1\n\nDELETE-PLAYLIST 1\nDelete Successful\n\nCREATE-PLAYLIST YOLDO 15 12\nPlaylist ID - 2\n\nMODIFY-PLAYLIST ADD-SONG 2 14 16\nPlaylist ID - 2\nPlaylist Name - YOLDO\nSong IDs - [15, 12, 14, 16]\n\nMODIFY-PLAYLIST ADD-SONG 0 1\nPlaylist ID - 0\nPlaylist Name - YOLO\nSong IDs - [1, 3]\n\nPLAY-PLAYLIST 2\n Song ID 	- 15\n Song Name 	- Lounge Act\n Genre 	- Rock\n Album 	- Nevermind\n Artists 	- Nirvana\n\nPLAY-SONG 16\n Song ID 	- 16\n Song Name 	- BLOOD.\n Genre 	- Hip-Hop\n Album 	- DAMN.\n Artists 	- Kendrick Lamar\n\nPLAY-SONG NEXT\n Song ID 	- 15\n Song Name 	- Lounge Act\n Genre 	- Rock\n Album 	- Nevermind\n Artists 	- Nirvana\n\nPLAY-SONG NEXT\n Song ID 	- 12\n Song Name 	- Lithium\n Genre 	- Rock\n Album 	- Nevermind\n Artists 	- Nirvana\n\nPLAY-SONG NEXT\n Song ID 	- 14\n Song Name 	- Stay Away\n Genre 	- Rock\n Album 	- Nevermind\n Artists 	- Nirvana\n\nPLAY-SONG BACK\n Song ID 	- 12\n Song Name 	- Lithium\n Genre 	- Rock\n Album 	- Nevermind\n Artists 	- Nirvana\n\nLIST-SONG GENRE Pop\n Song ID 	- 1\n Song Name 	- South of the Border\n Genre 	- Pop\n Album 	- No.6 Collaborations Project\n Album Artist 	- Ed Sheeran\n Artists 	- Ed Sheeran,Cardi.B,Camilla Cabello\n Song ID 	- 2\n Song Name 	- I Dont'Care\n Genre 	- Pop\n Album 	- No.6 Collaborations Project\n Album Artist 	- Ed Sheeran\n Artists 	- Ed Sheeran,Justin Bieber\n Song ID 	- 3\n Song Name 	- Remember The Name\n Genre 	- Pop\n Album 	- No.6 Collaborations Project\n Album Artist 	- Ed Sheeran\n Artists 	- Ed Sheeran,Eminem,50Cent\n Song ID 	- 4\n Song Name 	- Way To Break My Heart\n Genre 	- Pop\n Album 	- No.6 Collaborations Project\n Album Artist 	- Ed Sheeran\n Artists 	- Ed Sheeran,Skrillex\n Song ID 	- 5\n Song Name 	- Cross Me\n Genre 	- Pop\n Album 	- No.6 Collaborations Project\n Album Artist 	- Ed Sheeran\n Artists 	- Ed Sheeran,Chance The Rapper,PnB Rock\n\nLIST-SONG ARTIST Queen\n Song ID 	- 26\n Song Name 	- The Show Must Go on\n Genre 	- Rock\n Album 	- Innuendo\n Album Artist 	- Queen\n Artists 	- Queen\n Song ID 	- 27\n Song Name 	- Somebody To Love\n Genre 	- Rock\n Album 	- A Day at the Races\n Album Artist 	- Queen\n Artists 	- Queen\n Song ID 	- 28\n Song Name 	- Killer Queen\n Genre 	- Rock\n Album 	- Sheer Heart Attack\n Album Artist 	- Queen\n Artists 	- Queen\n Song ID 	- 29\n Song Name 	- Another One Bites the Dust\n Genre 	- Rock\n Album 	- The Game\n Album Artist 	- Queen\n Artists 	- Queen\n Song ID 	- 30\n Song Name 	- Bohemian Rhapsody\n Genre 	- Rock\n Album 	- A Night at the Opera\n Album Artist 	- Queen\n Artists 	- Queen\n\nLIST-SONG ALBUM Nevermind\n Song ID 	- 11\n Song Name 	- Something In the Way\n Genre 	- Rock\n Album 	- Nevermind\n Album Artist 	- Nirvana\n Artists 	- Nirvana\n Song ID 	- 12\n Song Name 	- Lithium\n Genre 	- Rock\n Album 	- Nevermind\n Album Artist 	- Nirvana\n Artists 	- Nirvana\n Song ID 	- 13\n Song Name 	- Come as You Are\n Genre 	- Rock\n Album 	- Nevermind\n Album Artist 	- Nirvana\n Artists 	- Nirvana\n Song ID 	- 14\n Song Name 	- Stay Away\n Genre 	- Rock\n Album 	- Nevermind\n Album Artist 	- Nirvana\n Artists 	- Nirvana\n Song ID 	- 15\n Song Name 	- Lounge Act\n Genre 	- Rock\n Album 	- Nevermind\n Album Artist 	- Nirvana\n Artists 	- Nirvana\n\nHISTORY 3\n Song ID 	- 12\n Song Name 	- Lithium\n Genre 	- Rock\n Album 	- Nevermind\n Album Artist 	- Nirvana\n Artists 	- Nirvana\n Song ID 	- 14\n Song Name 	- Stay Away\n Genre 	- Rock\n Album 	- Nevermind\n Album Artist 	- Nirvana\n Artists 	- Nirvana\n Song ID 	- 15\n Song Name 	- Lounge Act\n Genre 	- Rock\n Album 	- Nevermind\n Album Artist 	- Nirvana\n Artists 	- Nirvana\n Song ID 	- 16\n Song Name 	- BLOOD.\n Genre 	- Hip-Hop\n Album 	- DAMN.\n Album Artist 	- Kendrick Lamar\n Artists 	- Kendrick Lamar\n\nMOST-PLAYED\nSongs   No of Plays\nLithium  2\nLounge Act  2\nBLOOD.  1\nStay Away  1");

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);  
    }
}