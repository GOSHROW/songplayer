package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("PaylistTest")
public class PlaylistTest {
    
    @Test
    @DisplayName("sequenceFlow check")
    public void sequenceFlow_check(){
        Playlist playlist = new Playlist();
        ArrayList<Integer> actualReturnVals = new ArrayList<>();
        actualReturnVals.add(playlist.getCurrentSong());
        actualReturnVals.add(playlist.create(1, "Punk School", new ArrayList<>(Arrays.asList(1, 4, 6))));
        actualReturnVals.addAll(playlist.add(5));
        actualReturnVals.addAll(playlist.remove(4));
        actualReturnVals.addAll(playlist.remove(4));
        for (int i = 0; i < 4; ++i) {
            actualReturnVals.add(playlist.nextSong());
        }
        for (int i = 0; i < 4; ++i) {
            actualReturnVals.add(playlist.prevSong());
        }
        actualReturnVals.add(playlist.changeCurrent(5));

        ArrayList<Integer> expectedReturnVals = new ArrayList<>(Arrays.asList(-1, 1, 1, 4, 6, 5, 1, 6, 5, 1, 6, 5, 1, 6, 5, 1, 5, 6, 1, 5, 5));
        Assertions.assertEquals(expectedReturnVals, actualReturnVals);
    }
}