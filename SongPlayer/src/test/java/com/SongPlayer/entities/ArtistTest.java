package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("ArtistTest")
public class ArtistTest {
    
    @Test
    @DisplayName("getSongList method Should Return songList Given artistName")
    public void getSongList_method_Should_Return_songList_Given_artistName(){
        
        ArrayList<Integer> songIDList = new ArrayList<>(Arrays.asList(1, 5, 66));
        Artist artist = new Artist("Kinks");
        artist.addSong(1);
        artist.addSong(5);
        artist.addSong(66);
        
        ArrayList<Integer> actualSongList = artist.getSongList();
        ArrayList<Integer> expectedSongList = songIDList;
        
        Assertions.assertEquals(expectedSongList, actualSongList);
    }

}