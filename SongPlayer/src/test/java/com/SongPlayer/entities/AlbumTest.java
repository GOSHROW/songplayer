package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("AlbumTest")
public class AlbumTest {
    
    @Test
    @DisplayName("getSongList method Should Return songList Given albumName")
    public void getSongList_method_Should_Return_songList_Given_albumName(){
        
        ArrayList<Integer> songIDList = new ArrayList<>(Arrays.asList(1, 5, 66));
        Album album = new Album("SF Sorrow");
        album.addSong(1);
        album.addSong(5);
        album.addSong(66);
        
        ArrayList<Integer> actualSongList = album.getSongList();
        ArrayList<Integer> expectedSongList = songIDList;
        
        Assertions.assertEquals(expectedSongList, actualSongList);
    }

}