package com.SongPlayer.entities;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("SongTest")
public class SongTest {

    @Test
    @DisplayName("prettyOutForPoolQuery method Should Print song Information Given id")
    public void prettyOutForPoolQuery_method_Should_Print_song_Information_Given_id() throws IOException {
        
        Integer id = 1;
        String songName = "Sickle Clowns";
        String genre = "Psychedelic";
        String album = "Parachute";
        String owner = "The Pretty Things";
        ArrayList<String> artists =new ArrayList<>(Arrays.asList("The Pretty Things", "Dick Taylor"));
        Song song = new Song(id, songName, genre, album, owner, artists);

        // final PrintStream oldStdout = System.out;
        
        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        song.prettyOut();

        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println(" Song ID \t- " + id + "\n Song Name \t- " + songName + "\n Genre \t- " + genre + "\n Album \t- " +
            album + "\n Album Artist \t- " + owner + "\n Artists \t- " + String.join(",", artists));

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);    
    }

}