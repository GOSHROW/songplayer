package com.SongPlayer.entities;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("GenreTest")
public class GenreTest {
    
    @Test
    @DisplayName("getSongList method Should Return songList Given genreName")
    public void getSongList_method_Should_Return_songList_Given_genreName(){
        
        ArrayList<Integer> songIDList = new ArrayList<>(Arrays.asList(1, 5, 66));
        Genre genre = new Genre("Rock");
        genre.addSong(1);
        genre.addSong(5);
        genre.addSong(66);
        
        ArrayList<Integer> actualSongList = genre.getSongList();
        ArrayList<Integer> expectedSongList = songIDList;
        
        Assertions.assertEquals(expectedSongList, actualSongList);
    }

}