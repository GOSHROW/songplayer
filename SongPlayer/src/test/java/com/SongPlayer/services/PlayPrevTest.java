package com.SongPlayer.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.*;

@DisplayName("Play-Prev Test")
public class PlayPrevTest {

    @Test
    @DisplayName("Play Prev should print Song if valid")
    public void Play_Prev_should_print_Song_if_valid() throws SongNotInPool, IOException, PlaylistNotFound,
            NoSongsInPlaylist, SongNotInPlaylist, NoActivePlaylist, NoSongsInPlaylist {
        PoolRepository pool = new PoolRepository();
        UserRepository user = new UserRepository();
        pool.addNewSong(1, "YYZ", "Metal", "YYZ", "Rush", new ArrayList<String>(Arrays.asList("Neil Peart", "Geddy Lee")));
        pool.addNewSong(13, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
        CreatePlaylist createPlaylist = new CreatePlaylist();
        PlayPlaylist playPlaylist = new PlayPlaylist();
        PlayPrev playPrev = new PlayPrev();
        
        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        createPlaylist.createNewPlaylist(pool, user, "PIXIESSSS", new ArrayList<Integer>(Arrays.asList(1, 13)));
        playPlaylist.playAPlaylist(pool, user, 0);
        playPrev.playPrevSong(pool, user);

        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println("Playlist ID - 0\n Song ID 	- 1\n Song Name 	- YYZ\n Genre 	- Metal\n Album 	- YYZ\n Artists 	- Neil Peart,Geddy Lee\n Song ID 	- 13\n Song Name 	- Alec Eiffel\n Genre 	- Metal\n Album 	- Dolittle\n Artists 	- Kim Deal,Geddy Lee");

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);   
    }
}