package com.SongPlayer.services;

import com.SongPlayer.repositories.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SongsByArtistTest {
    @Test
    @DisplayName("SongsByArtist method Should Return songList Given artistName")
    public void SongsByArtist_method_Should_Return_songList_Given_artistName() throws IOException {
        PoolRepository pool = new PoolRepository();
        String artist = "Paul McCartney";
        pool.addNewSong(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney")));
        pool.addNewSong(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory")));
        pool.addNewSong(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney")));
        
        SongsByArtist songsByArtist = new SongsByArtist();
        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        songsByArtist.printSongsByArtist(pool, artist);

        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println(" Song ID 	- 2\n Song Name 	- Come On To Me\n Genre 	- Pop\n Album 	- Egypt Station\n Album Artist 	- Paul McCartney\n Artists 	- Paul McCartney,Linda McCartney\n Song ID 	- 25\n Song Name 	- Dominoes\n Genre 	- Rock\n Album 	- Egypt Station\n Album Artist 	- Paul McCartney\n Artists 	- Paul McCartney");
        
        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);      
    }
}