package com.SongPlayer.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import com.SongPlayer.exceptions.*;
import com.SongPlayer.repositories.*;

@DisplayName("Play-Playlist Test")
public class PlayPlaylistTest {

    @Test
    @DisplayName("Play Playlist should print First Song if valid")
    public void Play_Playlist_should_print_First_Song_if_valid() throws SongNotInPool, IOException, PlaylistNotFound,
            NoSongsInPlaylist {
        PoolRepository pool = new PoolRepository();
        UserRepository user = new UserRepository();
        pool.addNewSong(1, "YYZ", "Metal", "YYZ", "Rush", new ArrayList<String>(Arrays.asList("Neil Peart", "Geddy Lee")));
        pool.addNewSong(13, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
        CreatePlaylist createPlaylist = new CreatePlaylist();
        PlayPlaylist playPlaylist = new PlayPlaylist();
        
        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        createPlaylist.createNewPlaylist(pool, user, "PIXIESSSS", new ArrayList<Integer>(Arrays.asList(1, 13)));
        createPlaylist.createNewPlaylist(pool, user, "PIXIESSS", new ArrayList<Integer>(Arrays.asList(1)));
        playPlaylist.playAPlaylist(pool, user, 0);
        playPlaylist.playAPlaylist(pool, user, 1);



        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println("Playlist ID - 0\nPlaylist ID - 1\n Song ID 	- 1\n Song Name 	- YYZ\n Genre 	- Metal\n Album 	- YYZ\n Artists 	- Neil Peart,Geddy Lee\n Song ID 	- 1\n Song Name 	- YYZ\n Genre 	- Metal\n Album 	- YYZ\n Artists 	- Neil Peart,Geddy Lee");

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);   
    }
}