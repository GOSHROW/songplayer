package com.SongPlayer.services;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import com.SongPlayer.repositories.*;
import com.SongPlayer.exceptions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("ModPlaylist Test")
public class ModPlaylistTest {
    
    @Test
    @DisplayName("Get output for Adding to Playlist")
    public void Get_output_for_Adding_to_Playlist()  throws SongNotInPool, PlaylistNotFound, IOException {
        PoolRepository pool = new PoolRepository();
        UserRepository user = new UserRepository();
        pool.addNewSong(1, "YYZ", "Metal", "YYZ", "Rush", new ArrayList<String>(Arrays.asList("Neil Peart", "Geddy Lee")));
        pool.addNewSong(13, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
        pool.addNewSong(83, "Ana", "Funk", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Black Francis", "Joey Santiago", "David Loweing")));
        pool.addNewSong(173, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
        CreatePlaylist createPlaylist = new CreatePlaylist();
        ModPlaylist modPlaylist = new ModPlaylist();
        
        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        createPlaylist.createNewPlaylist(pool, user, "PIXIESSSS", new ArrayList<Integer>(Arrays.asList(1, 13)));
        modPlaylist.addToPlaylist(pool, user, 0, new ArrayList<>(Arrays.asList(83, 173)));

        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println("Playlist ID - 0\nPlaylist ID - 0\nPlaylist Name - PIXIESSSS\nSong IDs - [1, 13, 83, 173]");

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);   
        Assertions.assertEquals(true, true);
    }

    @Test
    @DisplayName("Get output for Deleting from Playlist")
    public void Get_output_for_Deleting_from_Playlist()  throws SongNotInPool, PlaylistNotFound, IOException {
        PoolRepository pool = new PoolRepository();
        UserRepository user = new UserRepository();
        pool.addNewSong(1, "YYZ", "Metal", "YYZ", "Rush", new ArrayList<String>(Arrays.asList("Neil Peart", "Geddy Lee")));
        pool.addNewSong(13, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
        pool.addNewSong(83, "Ana", "Funk", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Black Francis", "Joey Santiago", "David Loweing")));
        pool.addNewSong(173, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
        CreatePlaylist createPlaylist = new CreatePlaylist();
        ModPlaylist modPlaylist = new ModPlaylist();
        
        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        createPlaylist.createNewPlaylist(pool, user, "PIXIESSSS", new ArrayList<Integer>(Arrays.asList(1, 13, 83, 173)));
        modPlaylist.delFromPlaylist(pool, user, 0, new ArrayList<>(Arrays.asList(83, 173)));

        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println("Playlist ID - 0\nPlaylist ID - 0\nPlaylist Name - PIXIESSSS\nSong IDs - [1, 13]");

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);   
    }

}