package com.SongPlayer.services;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import com.SongPlayer.exceptions.SongNotInPool;
import com.SongPlayer.repositories.PoolRepository;
import com.SongPlayer.repositories.UserRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("CreatePlaylist Test")
public class CreatePlaylistTest {

    @Test
    @DisplayName("Create Playlist should print New ID if valid")
    public void Create_Playlist_should_print_New_ID_if_valid() throws SongNotInPool, IOException {
        PoolRepository pool = new PoolRepository();
        UserRepository user = new UserRepository();
        pool.addNewSong(1, "YYZ", "Metal", "YYZ", "Rush", new ArrayList<String>(Arrays.asList("Neil Peart", "Geddy Lee")));
        pool.addNewSong(13, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
        CreatePlaylist createPlaylist = new CreatePlaylist();
        
        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        createPlaylist.createNewPlaylist(pool, user, "PIXIESSSS", new ArrayList<Integer>(Arrays.asList(1, 13)));
        createPlaylist.createNewPlaylist(pool, user, "PIXIESSS", new ArrayList<Integer>(Arrays.asList(1)));
        createPlaylist.createNewPlaylist(pool, user, "PIXI", new ArrayList<Integer>(Arrays.asList(13)));


        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println("Playlist ID - 0\nPlaylist ID - 1\nPlaylist ID - 2");

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);   
    }

    // @Test
    // @DisplayName("Create Playlist should give exception if invalid")
    // public void Create_Playlist_should_give_Exception_if_invalid() throws SongNotInPool, IOException {
    //     PoolRepository pool = new PoolRepository();
    //     UserRepository user = new UserRepository();
    //     pool.addNewSong(1, "YYZ", "Metal", "YYZ", "Rush", new ArrayList<String>(Arrays.asList("Neil Peart", "Geddy Lee")));
    //     pool.addNewSong(13, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
    //     CreatePlaylist createPlaylist = new CreatePlaylist();
        
    //     String pathString = "./temp/actual-file.test.txt";
    //     Path actualPath = Paths.get(pathString);
    //     final File actualFile = new File(pathString);
    //     System.setOut(new PrintStream(actualFile));
    //     createPlaylist.createNewPlaylist(pool, user, "PIXIESSSS", new ArrayList<Integer>(Arrays.asList(1, 2)));

    //     pathString = "./temp/expected-file.test.txt";
    //     Path expectedPath = Paths.get(pathString);
    //     final File expectedFile = new File(pathString);
    //     System.setOut(new PrintStream(expectedFile));
    //     System.out.println("No Such Song found!");

    //     byte[] expectedByteContent = Files.readAllBytes(expectedPath);
    //     byte[] actualByteContent = Files.readAllBytes(actualPath);
    //     Assertions.assertArrayEquals(expectedByteContent, actualByteContent);   
    // }
}