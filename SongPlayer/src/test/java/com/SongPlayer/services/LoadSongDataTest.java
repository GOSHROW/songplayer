package com.SongPlayer.services;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.SongPlayer.repositories.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Load SongData Test")
public class LoadSongDataTest {

    @Test
    @DisplayName("Check Pool is formed on Data Loading")
    public void Check_Pool_is_formed_on_Data_loading() throws IOException {
        String pathCSVStr = "./src/main/java/com/SongPlayer/repositories/data/songs.csv";

        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        System.setOut(new PrintStream(actualFile));
        LoadSongData loadSongData = new LoadSongData();
        PoolRepository pool = loadSongData.CreatePool(pathCSVStr);
        System.out.println(pool.toString());

        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println("Pool{songMap: {1=Song{ID='1'songName='South of the Border'genre='Pop'album='No.6 Collaborations Project'owner='Ed Sheeran'artist='[Ed Sheeran, Cardi.B, Camilla Cabello]'}, 2=Song{ID='2'songName='I Dont'Care'genre='Pop'album='No.6 Collaborations Project'owner='Ed Sheeran'artist='[Ed Sheeran, Justin Bieber]'}, 3=Song{ID='3'songName='Remember The Name'genre='Pop'album='No.6 Collaborations Project'owner='Ed Sheeran'artist='[Ed Sheeran, Eminem, 50Cent]'}, 4=Song{ID='4'songName='Way To Break My Heart'genre='Pop'album='No.6 Collaborations Project'owner='Ed Sheeran'artist='[Ed Sheeran, Skrillex]'}, 5=Song{ID='5'songName='Cross Me'genre='Pop'album='No.6 Collaborations Project'owner='Ed Sheeran'artist='[Ed Sheeran, Chance The Rapper, PnB Rock]'}, 6=Song{ID='6'songName='Give Life Back To Music'genre='Electronic Dance Music'album='Random Access Memories'owner='Daft Punk'artist='[Daft Punk, Nile Rodgers]'}, 7=Song{ID='7'songName='Instant Crush'genre='Electronic Dance Music'album='Random Access Memories'owner='Daft Punk'artist='[Daft Punk, Julian Casablancas]'}, 8=Song{ID='8'songName='Get Lucky'genre='Electronic Dance Music'album='Random Access Memories'owner='Daft Punk'artist='[Daft Punk, Nile Rodgers, Pharrell Williams]'}, 9=Song{ID='9'songName='Lose Yourself To Dance'genre='Electronic Dance Music'album='Random Access Memories'owner='Daft Punk'artist='[Daft Punk, Nile Rodgers, Pharrell William]'}, 10=Song{ID='10'songName='Giorgio by Moroder'genre='Electronic Dance Music'album='Random Access Memories'owner='Daft Punk'artist='[Daft Punk, Giorgio Moroder]'}, 11=Song{ID='11'songName='Something In the Way'genre='Rock'album='Nevermind'owner='Nirvana'artist='[Nirvana]'}, 12=Song{ID='12'songName='Lithium'genre='Rock'album='Nevermind'owner='Nirvana'artist='[Nirvana]'}, 13=Song{ID='13'songName='Come as You Are'genre='Rock'album='Nevermind'owner='Nirvana'artist='[Nirvana]'}, 14=Song{ID='14'songName='Stay Away'genre='Rock'album='Nevermind'owner='Nirvana'artist='[Nirvana]'}, 15=Song{ID='15'songName='Lounge Act'genre='Rock'album='Nevermind'owner='Nirvana'artist='[Nirvana]'}, 16=Song{ID='16'songName='BLOOD.'genre='Hip-Hop'album='DAMN.'owner='Kendrick Lamar'artist='[Kendrick Lamar]'}, 17=Song{ID='17'songName='GOD.'genre='Hip-Hop'album='DAMN.'owner='Kendrick Lamar'artist='[Kendrick Lamar]'}, 18=Song{ID='18'songName='PRIDE.'genre='Hip-Hop'album='DAMN.'owner='Kendrick Lamar'artist='[Kendrick Lamar]'}, 19=Song{ID='19'songName='YAH.'genre='Hip-Hop'album='DAMN.'owner='Kendrick Lamar'artist='[Kendrick Lamar]'}, 20=Song{ID='20'songName='DUCKWORTH.'genre='Hip-Hop'album='DAMN.'owner='Kendrick Lamar'artist='[Kendrick Lamar]'}, 21=Song{ID='21'songName='Flamenco Sketches'genre='Jazz'album='Kind of Blue'owner='Miles Davis'artist='[Miles Davis]'}, 22=Song{ID='22'songName='So What'genre='Jazz'album='Kind of Blue'owner='Miles Davis'artist='[Miles Davis]'}, 23=Song{ID='23'songName='Freddie Freeloader'genre='Jazz'album='Kind of Blue'owner='Miles Davis'artist='[Miles Davis]'}, 24=Song{ID='24'songName='Blue in Green'genre='Jazz'album='Kind of Blue'owner='Miles Davis'artist='[Miles Davis]'}, 25=Song{ID='25'songName='All Blues'genre='Jazz'album='Kind of Blue'owner='Miles Davis'artist='[Miles Davis]'}, 26=Song{ID='26'songName='The Show Must Go on'genre='Rock'album='Innuendo'owner='Queen'artist='[Queen]'}, 27=Song{ID='27'songName='Somebody To Love'genre='Rock'album='A Day at the Races'owner='Queen'artist='[Queen]'}, 28=Song{ID='28'songName='Killer Queen'genre='Rock'album='Sheer Heart Attack'owner='Queen'artist='[Queen]'}, 29=Song{ID='29'songName='Another One Bites the Dust'genre='Rock'album='The Game'owner='Queen'artist='[Queen]'}, 30=Song{ID='30'songName='Bohemian Rhapsody'genre='Rock'album='A Night at the Opera'owner='Queen'artist='[Queen]'}}, genreMap: {Pop=Genre{genreName='Pop'songList='[1, 2, 3, 4, 5]'}, Rock=Genre{genreName='Rock'songList='[11, 12, 13, 14, 15, 26, 27, 28, 29, 30]'}, Jazz=Genre{genreName='Jazz'songList='[21, 22, 23, 24, 25]'}, Electronic Dance Music=Genre{genreName='Electronic Dance Music'songList='[6, 7, 8, 9, 10]'}, Hip-Hop=Genre{genreName='Hip-Hop'songList='[16, 17, 18, 19, 20]'}}, albumMap: {A Night at the Opera=Album{albumName='A Night at the Opera'songList='[30]'}, A Day at the Races=Album{albumName='A Day at the Races'songList='[27]'}, No.6 Collaborations Project=Album{albumName='No.6 Collaborations Project'songList='[1, 2, 3, 4, 5]'}, Random Access Memories=Album{albumName='Random Access Memories'songList='[6, 7, 8, 9, 10]'}, Sheer Heart Attack=Album{albumName='Sheer Heart Attack'songList='[28]'}, Innuendo=Album{albumName='Innuendo'songList='[26]'}, The Game=Album{albumName='The Game'songList='[29]'}, Kind of Blue=Album{albumName='Kind of Blue'songList='[21, 22, 23, 24, 25]'}, Nevermind=Album{albumName='Nevermind'songList='[11, 12, 13, 14, 15]'}, DAMN.=Album{albumName='DAMN.'songList='[16, 17, 18, 19, 20]'}}, artistMap: {Ed Sheeran=Artist{artistName='Ed Sheeran'songList='[1, 2, 3, 4, 5]'}, Daft Punk=Artist{artistName='Daft Punk'songList='[6, 7, 8, 9, 10]'}, Pharrell Williams=Artist{artistName='Pharrell Williams'songList='[8]'}, Justin Bieber=Artist{artistName='Justin Bieber'songList='[2]'}, Nirvana=Artist{artistName='Nirvana'songList='[11, 12, 13, 14, 15]'}, Queen=Artist{artistName='Queen'songList='[26, 27, 28, 29, 30]'}, Chance The Rapper=Artist{artistName='Chance The Rapper'songList='[5]'}, Giorgio Moroder=Artist{artistName='Giorgio Moroder'songList='[10]'}, Cardi.B=Artist{artistName='Cardi.B'songList='[1]'}, Julian Casablancas=Artist{artistName='Julian Casablancas'songList='[7]'}, Nile Rodgers=Artist{artistName='Nile Rodgers'songList='[6, 8, 9]'}, Miles Davis=Artist{artistName='Miles Davis'songList='[21, 22, 23, 24, 25]'}, PnB Rock=Artist{artistName='PnB Rock'songList='[5]'}, Camilla Cabello=Artist{artistName='Camilla Cabello'songList='[1]'}, 50Cent=Artist{artistName='50Cent'songList='[3]'}, Eminem=Artist{artistName='Eminem'songList='[3]'}, Kendrick Lamar=Artist{artistName='Kendrick Lamar'songList='[16, 17, 18, 19, 20]'}, Skrillex=Artist{artistName='Skrillex'songList='[4]'}, Pharrell William=Artist{artistName='Pharrell William'songList='[9]'}}, }");

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent);      
    }
}