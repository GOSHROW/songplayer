package com.SongPlayer.services;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import com.SongPlayer.exceptions.NoActivePlaylist;
import com.SongPlayer.exceptions.NoSongsInPlaylist;
import com.SongPlayer.exceptions.PlaylistNotFound;
import com.SongPlayer.exceptions.SongNotInPool;
import com.SongPlayer.repositories.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("MostPlayed10 Test")
public class MostPlayed10Test {

    @Test
    @DisplayName("print10MostPlayed Method should print 10 Most Played Songs for Given User")
    public void print10MostPlayed_Method_should_print_10_Most_Played_Songs_for_Given_User()
            throws SongNotInPool, PlaylistNotFound, NoSongsInPlaylist, NoActivePlaylist, IOException {
        PoolRepository pool = new PoolRepository();
        UserRepository user = new UserRepository();
        pool.addNewSong(1, "YYZ", "Metal", "YYZ", "Rush", new ArrayList<String>(Arrays.asList("Neil Peart", "Geddy Lee")));
        pool.addNewSong(13, "Alec Eiffel", "Metal", "Dolittle", "Pixies", new ArrayList<String>(Arrays.asList("Kim Deal", "Geddy Lee")));
        CreatePlaylist createPlaylist = new CreatePlaylist();
        PlayPlaylist playPlaylist = new PlayPlaylist();
        PlayPrev playPrev = new PlayPrev();
        MostPlayed10 mostPlayed10 = new MostPlayed10();

        String pathString = "./temp/actual-file.test.txt";
        Path actualPath = Paths.get(pathString);
        final File actualFile = new File(pathString);
        createPlaylist.createNewPlaylist(pool, user, "PIXIESSSS", new ArrayList<Integer>(Arrays.asList(1, 13)));
        playPlaylist.playAPlaylist(pool, user, 0);
        playPrev.playPrevSong(pool, user);
        playPrev.playPrevSong(pool, user);
        System.setOut(new PrintStream(actualFile));
        mostPlayed10.print10MostPlayed(pool, user);

        pathString = "./temp/expected-file.test.txt";
        Path expectedPath = Paths.get(pathString);
        final File expectedFile = new File(pathString);
        System.setOut(new PrintStream(expectedFile));
        System.out.println("Songs   No of Plays\nYYZ  2\nAlec Eiffel  1");

        byte[] expectedByteContent = Files.readAllBytes(expectedPath);
        byte[] actualByteContent = Files.readAllBytes(actualPath);
        Assertions.assertArrayEquals(expectedByteContent, actualByteContent); 
        
    }
}