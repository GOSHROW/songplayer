package com.SongPlayer.repositories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.SongPlayer.exceptions.PlaylistNotFound;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("UserRepositoryTest")
public class UserRepositoryTest {

    @Test
    @DisplayName("Check User Actions in History Logging")
    public void Check_User_Actions_in_History_Logging() {
        UserRepository user = new UserRepository();
        user.playedASong(5);
        user.playedASong(1);
        user.playedASong(3);
        user.playedASong(2);
        user.playedASong(5);
        ArrayList<Integer> actualHistory = user.getHistory();

        ArrayList<Integer> expectedHistory = new ArrayList<>(Arrays.asList(5, 2, 3, 1));

        Assertions.assertEquals(expectedHistory, actualHistory);
    }

    @Test
    @DisplayName("Check User Actions in get10MostPlayed Logging")
    public void Check_User_Actions_in_get10MostPlayed_Logging() {
        UserRepository user = new UserRepository();
        user.playedASong(5);
        user.playedASong(1);
        user.playedASong(3);
        user.playedASong(2);
        user.playedASong(5);
        List<List<Integer>> actualHistory = user.get10MostPlayed();

        List<List<Integer>> expectedHistory = Arrays.asList(Arrays.asList(5, 2), Arrays.asList(1, 1), Arrays.asList(2, 1), Arrays.asList(3, 1));

        Assertions.assertEquals(expectedHistory, actualHistory);
    }

    @Test
    @DisplayName("Check User Playlist Handling")
    public void Check_User_Playlist_Handling() throws PlaylistNotFound {
        UserRepository user = new UserRepository();
        ArrayList<Integer> actualResults = new ArrayList<>();
        actualResults.add(user.createPlaylist("Paperback", new ArrayList<>(Arrays.asList(2, 4))));
        actualResults.add(user.createPlaylist("Martha", new ArrayList<>(Arrays.asList(3, 4))));
        if (user.deletePlaylist(1)) {
            actualResults.add(user.getCurrentPlaylist());
        }
        if (user.deletePlaylist(1)) {
            actualResults.add(user.getCurrentPlaylist());
        }
        actualResults.add(user.createPlaylist("Marthas", new ArrayList<>(Arrays.asList(3, 1, 5))));
        user.switchPlaylist(0);
        actualResults.add(user.getCurrentPlaylist());

        ArrayList<Integer> expectedResults = new ArrayList<>(Arrays.asList(0, 1, -1, 2, 0));

        Assertions.assertEquals(expectedResults, actualResults);
    }

}