package com.SongPlayer.repositories;

import com.SongPlayer.entities.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("PoolRepositoryTest")
public class PoolRepositoryTest {
    
    @Test
    @DisplayName("getByGenre method Should Return songList Given genreName")
    public void getByGenre_method_Should_Return_songList_Given_genreName(){
        PoolRepository pool = new PoolRepository();
        String genre = "Rock";
        pool.addNewSong(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney")));
        pool.addNewSong(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory")));
        pool.addNewSong(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney")));
        ArrayList<Song> actualSongList = pool.getByGenre(genre);

        ArrayList<Song> songs = new ArrayList<>();
        songs.add(new Song(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney"))));
        songs.add(new Song(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory"))));
        songs.add(new Song(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney"))));
        ArrayList<Song> expectedSongList = new ArrayList<>();
        expectedSongList.add(songs.get(1));
        expectedSongList.add(songs.get(2));
        
        Assertions.assertArrayEquals(expectedSongList.toArray(), actualSongList.toArray());
    }

    @Test
    @DisplayName("getByGenre method Should Return Empty Given Invalid")
    public void getByGenre_method_Should_Return_Empty_Given_Invalid(){
        PoolRepository pool = new PoolRepository();
        String genre = "Country";
        pool.addNewSong(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney")));
        pool.addNewSong(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory")));
        pool.addNewSong(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney")));
        ArrayList<Song> actualSongList = pool.getByGenre(genre);

        ArrayList<Song> songs = new ArrayList<>();
        songs.add(new Song(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney"))));
        songs.add(new Song(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory"))));
        songs.add(new Song(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney"))));
        ArrayList<Song> expectedSongList = new ArrayList<>();
        
        Assertions.assertArrayEquals(expectedSongList.toArray(), actualSongList.toArray());
    }    


    @Test
    @DisplayName("getByAlbum method Should Return songList Given albumName")
    public void getByAlbum_method_Should_Return_songList_Given_albumName(){
        PoolRepository pool = new PoolRepository();
        String album = "Egypt Station";
        pool.addNewSong(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney")));
        pool.addNewSong(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory")));
        pool.addNewSong(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney")));
        ArrayList<Song> actualSongList = pool.getByAlbum(album);

        ArrayList<Song> songs = new ArrayList<>();
        songs.add(new Song(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney"))));
        songs.add(new Song(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory"))));
        songs.add(new Song(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney"))));
        ArrayList<Song> expectedSongList = new ArrayList<>();
        expectedSongList.add(songs.get(0));
        expectedSongList.add(songs.get(2));
        
        Assertions.assertArrayEquals(expectedSongList.toArray(), actualSongList.toArray());
    }

    @Test
    @DisplayName("getByAlbum method Should Return songList Given albumName")
    public void getByAlbum_method_Should_Return_Empty_Given_Invalid(){
        PoolRepository pool = new PoolRepository();
        String album = "McCartney";
        pool.addNewSong(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney")));
        pool.addNewSong(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory")));
        pool.addNewSong(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney")));
        ArrayList<Song> actualSongList = pool.getByAlbum(album);

        ArrayList<Song> songs = new ArrayList<>();
        songs.add(new Song(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney"))));
        songs.add(new Song(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory"))));
        songs.add(new Song(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney"))));
        ArrayList<Song> expectedSongList = new ArrayList<>();
        
        Assertions.assertArrayEquals(expectedSongList.toArray(), actualSongList.toArray());
    }    
    
    
    @Test
    @DisplayName("getByArtist method Should Return songList Given artistName")
    public void getByArtist_method_Should_Return_songList_Given_artistName(){
        PoolRepository pool = new PoolRepository();
        String artist = "Paul McCartney";
        pool.addNewSong(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney")));
        pool.addNewSong(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory")));
        pool.addNewSong(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney")));
        ArrayList<Song> actualSongList = pool.getByArtist(artist);

        ArrayList<Song> songs = new ArrayList<>();
        songs.add(new Song(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney"))));
        songs.add(new Song(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory"))));
        songs.add(new Song(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney"))));
        ArrayList<Song> expectedSongList = new ArrayList<>();
        expectedSongList.add(songs.get(0));
        expectedSongList.add(songs.get(2));
        
        Assertions.assertArrayEquals(expectedSongList.toArray(), actualSongList.toArray());
    }

    @Test
    @DisplayName("getByArtist method Should Return songList Given artistName")
    public void getByArtist_method_Should_Return_Empty_Given_Invalid(){
        PoolRepository pool = new PoolRepository();
        String artist = "John Lennon";
        pool.addNewSong(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney")));
        pool.addNewSong(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory")));
        pool.addNewSong(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney")));
        ArrayList<Song> actualSongList = pool.getByArtist(artist);

        ArrayList<Song> songs = new ArrayList<>();
        songs.add(new Song(2, "Come On To Me", "Pop", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney", "Linda McCartney"))));
        songs.add(new Song(18, "This Time Tommorow", "Rock", "Lola", "Ray Davies", new ArrayList<String>(Arrays.asList("Ray Davies", "Dave Davies", "Mick Avory"))));
        songs.add(new Song(25, "Dominoes", "Rock", "Egypt Station", "Paul McCartney", new ArrayList<String>(Arrays.asList("Paul McCartney"))));
        ArrayList<Song> expectedSongList = new ArrayList<>();
        
        Assertions.assertArrayEquals(expectedSongList.toArray(), actualSongList.toArray());
    }
}