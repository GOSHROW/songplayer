# Song Player

The implementation of the Code is based upon [the provided requirements](https://docs.google.com/document/d/16S9z2PuqKvUK6CqsZapb7bUae0x71AJ40B2t0z8aPHY)

The codebase is roughly broken down as

- Entities
  - Album
  - Artist
  - Genre
  - Playlist 
  - Song
- Repositories
  - Pool
  - User
- Services
  - CreatePlaylist
  - DeletePlaylist
  - History
  - LoadSongData
  - ModPlaylist
  - PlayNext
  - PlayPlaylist
  - PlayPrev
  - PlaySong
  - SongsByAlbum
  - SongsByArtist
  - SongsByGenre
- Exceptions
  - ExpectedMoreArgs
  - NoActivePlaylist
  - NoSongsInPlaylist
  - NoSuchCommandException
  - PlaylistNotFound
  - SongNotInPlaylist
  - SongNotInPool
- Commands
  - CreatePlaylistCommand
  - DeletePlaylistCommand
  - HistoryCommand
  - ListSongsCommand
  - ModPlaylistCommand
  - PlayPlaylistCommand
  - PlaySongCommand
  - ICommand
  - CommandInvoker
- Config
  - ApplicationConfig
- Util

## Testing the Implementation

1. Clone this Repo to Local System
1. Check for Build Configs of the Gradle from build.gradle
1. To check out a demo of the code and all provided functionalities, open up the file at
`SongPlayer/src/test/java/com/SongPlayer/config/IntegrationTest.java`
1. Run Test on the method
`check_Integration_Across_Valid_Commands`
1. Add your custom commands as per the requirements, to 
`ArrayList<String> commands` or utilize the `ApplicationConfig` for your own Custom Usage.
1. In case, 
`check_Integration_Across_Valid_Commands` is run, the output maybe properly observed at `SongPlayer/temp/actual-file.test.txt`

### ToDo

~~Optional Requirement~~

___

Swapnil Ghosh
